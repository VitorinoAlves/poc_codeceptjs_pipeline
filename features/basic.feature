Feature: Business rules
  In order to achieve my goals
  As a persona
  I want to be able to interact with a system

  Scenario: Accesses the Quickstart page
    Given the user has accessed the codeceptJS home page
    When the user clicks in the Quickstart option
    Then the system redirect the user to the Quickstart option
