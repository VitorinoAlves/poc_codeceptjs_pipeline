const { I } = inject();
// Add in your custom step files

Given('the user has accessed the codeceptJS home page', () => {
  I.amOnPage('');
});

When('the user clicks in the Quickstart option', () => {
  I.click('Quickstart');
});

Then('the system redirect the user to the Quickstart option', () => {
  I.see('QuickstartAAAfdegr');
});
